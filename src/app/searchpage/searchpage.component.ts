import {Component, OnDestroy, OnInit} from '@angular/core';
import {RandomMovieService} from '../services/random-movie.service';
import {MovieList} from '../models/movieList';
import {Movie} from '../models/movie';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-searchpage',
  templateUrl: './searchpage.component.html',
  styleUrls: ['./searchpage.component.scss']
})
export class SearchpageComponent implements OnInit, OnDestroy{
  page = 1;
  movies: Movie[];
  totalResults: number;
  searchText: string;
  text: string;

  constructor(private Activatedroute: ActivatedRoute,
              private router: Router,
              private movieService: RandomMovieService) {
    this.movies = new Array<Movie>();
  }

  sub;

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.sub = this.Activatedroute.paramMap.subscribe(params => {
      this.text = params.get('text');
      this.getMovies(this.text, this.page);
    });
  }

  // tslint:disable-next-line:typedef
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onBack(): void {
    this.router.navigate(['home']);
  }

  // tslint:disable-next-line:typedef
  getMovies(searchText: string, page: number){
    this.searchText = searchText;
    this.movieService.getData(searchText, page).subscribe((data: MovieList) => {
      this.totalResults = data.totalResults;
      this.movies = data.Search;
    });
  }
}

