import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MovieList} from '../models/movieList';
import {Movie} from '../models/movie';

export interface MovieHelp {
  Response: string;
  Search: Movie[];
  totalResults: string;
  Error: string;
}

@Injectable({
  providedIn: 'root'
})
export class RandomMovieService {
  url = 'http://www.omdbapi.com/?apikey=47b43e5';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line:typedef
  getData(text: string, page: number): Observable<MovieList> {
    return this.http.get<MovieList>(this.url + '&s=' + text + '&page=' + page);
  }

  getDataById(id: string): Observable<Movie> {
    return this.http.get<Movie>(this.url + '&i=' + id + '&plot=full');
  }
}

