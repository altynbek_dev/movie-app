export class Movie {
  Title: string;
  Year: number;
  Poster: string;
  Type: string;
  imdbID: string;
  Plot: string;
  Genre: string;
  Director: string;
  Writer: string;
  Actors: string;
  Language: string;
  Country: string;
  Awards: string;
  imdbRating: string;
  Runtime: string;
}
