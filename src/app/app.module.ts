import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgxPaginationModule} from 'ngx-pagination';

import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {RouterModule, Routes} from '@angular/router';
import {HeaderComponent} from './header/header.component';
import {SearchpageComponent} from './searchpage/searchpage.component';
import {HomeComponent} from './home/home.component';
import {HttpClientModule} from '@angular/common/http';
import {MovieComponent} from './movie/movie.component';
import {FooterComponent} from './footer/footer.component';
import {MovieDetailsComponent} from './movie-details/movie-details.component';
import {FormsModule} from '@angular/forms';
import {ToastrModule} from 'ngx-toastr';
import {LoginComponent} from './login/login.component';
import {RegisterPageComponent} from './register-page/register-page.component';


const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'search/:text', component: SearchpageComponent},
  {path: 'movie/:id', component: MovieDetailsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterPageComponent},
  {path: 'admin', loadChildren: './admin/admin.module#AdminModule'}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchpageComponent,
    HomeComponent,
    MovieComponent,
    FooterComponent,
    MovieDetailsComponent,
    LoginComponent,
    RegisterPageComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    NgxPaginationModule,
    FormsModule,
    ToastrModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
