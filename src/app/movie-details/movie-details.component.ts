import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Movie} from '../models/movie';
import {ActivatedRoute, Router} from '@angular/router';
import {RandomMovieService} from '../services/random-movie.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit, OnDestroy {
  id: string;
  movie: Movie;

  constructor(private Activatedroute: ActivatedRoute,
              private router: Router,
              private movieService: RandomMovieService) {
  }

  sub;

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.sub = this.Activatedroute.paramMap.subscribe(params => {
      console.log(params);
      this.id = params.get('id');
      this.movieService.getDataById(this.id).subscribe(data => this.movie = data);
    });
  }

  // tslint:disable-next-line:typedef
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onBack(): void {
    this.router.navigate(['search']);
  }

}
